# MongoDB (資料庫)

### package.json

<hr>

```json
"dependencies": {
    "@his-base/mongo-base": "git+https://gitlab.com/his5252030/base/mongo-base.git"
}
```

### Module

<hr>

在 module 的 providers 傳入 MongoBase 用 useFactory 進行設定<br><br>
引入 ConfigModule、ConfigService 來使用**環境變數**<br>
若為全域環境變數可以在其他地方直接 constructor ConfigService，無須 import ConfigModule<br>
關於 config 的設定可以觀看[這篇](https://docs.nestjs.com/techniques/configuration/ 'Title')

```ts
import { MongoBaseService } from '@his-base/mongo-base/dist';
import { ConfigModule, ConfigService } from '@nestjs/config';

                 //isGlobal可以將環境變數變為全域，沒加就只限於這個 Module
imports: [ConfigModule.forRoot({ isGlobal: true })],
providers: [
  {
    provide: MongoBaseService,
    useFactory: (config: ConfigService) => {
      return new MongoBaseService(
        //獲取環境變數
        config.get('MONGO_URL'),
        config.get('MONGO_DATABASE'),
        config.get('MONGO_COLLECTION'),
      );
    },
    //注入config
    inject: [ConfigService],
  },
];
```

### 建立環境變數(.env)

<hr>

```.env
MONGO_URL=mongodb://localhost:27017
MONGO_DATABASE=practice
MONGO_COLLECTION=first
```

### Controller

<hr>

在 controller 實體化 mongoDB 並操作

```ts
import { MongoBaseService } from '@his-base/mongo-base/dist';

export class MongoController {

  constructor(private readonly mongoDB: MongoBaseService) {}

  //找單筆資料
  this.mongoDB.findDocument(id)

  //找多筆資料
  this.mongoDB.findDocuments(filter, options?)

  //新增單體資料
  this.mongoDB.insertDocument(document)

  //更新單筆資料
  this.mongoDB.updateDocument(id,update)

  //刪除單筆資料
  this.mongoDB.deleteDocument(id)

  //聚合資料
  this.mondoDB.aggregateDocuments(pipeline?, options?)
}
```
