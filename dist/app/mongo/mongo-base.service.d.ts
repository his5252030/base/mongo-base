import { Document, InsertOneResult, DeleteResult, WithId, UpdateResult, Filter, OptionalId, FindOptions } from 'mongodb';
export declare class MongoBaseService {
    #private;
    constructor(url: string, databaseName: string, collectionName: string);
    insertDocument(document: OptionalId<Document>): Promise<InsertOneResult<Document>>;
    deleteDocument(id: string): Promise<DeleteResult>;
    updateDocument(id: string, document: Document): Promise<Document | UpdateResult>;
    findDocument(id: string): Promise<WithId<Document>>;
    findDocuments(filter: Filter<Document>, options?: FindOptions<Document>): Promise<WithId<Document>[]>;
    aggregateDocuments(pipeline?: Document[], options?: FindOptions<Document>): Promise<Document[]>;
}
