"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoBaseService = void 0;
const common_1 = require("@nestjs/common");
const mongodb_1 = require("mongodb");
let MongoBaseService = exports.MongoBaseService = class MongoBaseService {
    #client;
    #databaseName;
    #collectionName;
    constructor(url, databaseName, collectionName) {
        this.#client = new mongodb_1.MongoClient(url);
        this.#databaseName = databaseName;
        this.#collectionName = collectionName;
    }
    async insertDocument(document) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.insertOne(document);
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
    async deleteDocument(id) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.deleteOne({
                _id: new mongodb_1.ObjectId(id),
            });
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
    async updateDocument(id, document) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.updateOne({ _id: new mongodb_1.ObjectId(id) }, { $set: document });
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
    async findDocument(id) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.findOne({
                _id: new mongodb_1.ObjectId(id),
            });
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
    async findDocuments(filter, options) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.find(filter, options).toArray();
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
    async aggregateDocuments(pipeline, options) {
        try {
            await this.#client.connect();
            const database = this.#client.db(this.#databaseName);
            const collection = database.collection(this.#collectionName);
            const result = await collection.aggregate(pipeline, options).toArray();
            return result;
        }
        finally {
            await this.#client.close();
        }
    }
};
exports.MongoBaseService = MongoBaseService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [String, String, String])
], MongoBaseService);
//# sourceMappingURL=mongo-base.service.js.map