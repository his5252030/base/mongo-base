"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const mongo_base_service_1 = require("./mongo-base.service");
describe('MongoBaseService', () => {
    let service;
    const id = '64cccc19487deeee6440aa69';
    const document = {
        _id: new mongodb_1.ObjectId('64cccc19487deeee6440aa69'),
        name: 'test',
        value: 100,
    };
    let newDocument;
    beforeAll(async () => {
        service = new mongo_base_service_1.MongoBaseService('mongodb://localhost:27017', 'practice', 'tests');
    });
    describe('insertDocument', () => {
        it('should insert a document into the collection', async () => {
            const insert = await service.insertDocument(document);
            expect(insert).toBeTruthy();
        });
    });
    describe('findDocument', () => {
        it('should find a document by _id', async () => {
            newDocument = await service.findDocument(id);
            expect(newDocument).toEqual(document);
        });
    });
    describe('updateDocument', () => {
        it('should update a document in the collection', async () => {
            await service.updateDocument(id, { value: 200 });
            newDocument = await service.findDocument(id);
            expect(newDocument.value).toEqual(200);
        });
    });
    describe('findDocuments', () => {
        it('should find all documents that match the filter', async () => {
            const foundDocuments = await service.findDocuments({});
            expect(foundDocuments).toEqual([newDocument]);
        });
    });
    describe('deleteDocument', () => {
        it('should delete a document from the collection', async () => {
            await service.deleteDocument(id);
            const foundDocument = await service.findDocument(id);
            expect(foundDocument).toBeNull();
        });
    });
});
//# sourceMappingURL=mongo-base.service.test.js.map