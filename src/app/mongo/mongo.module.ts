import { Module } from '@nestjs/common';
import { MongoBaseService } from './mongo-base.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true })],
  controllers: [],
  providers: [
    {
      provide: MongoBaseService,
      useFactory: async (config: ConfigService) => {
        return new MongoBaseService(
          config.get('MONGO_URL'),
          config.get('MONGO_DATABASE'),
          config.get('MONGO_COLLECTION'),
        );
      },
      inject: [ConfigService],
    },
  ],
})
export class MongoModule {}
