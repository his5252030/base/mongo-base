import { ObjectId } from 'mongodb';
import { MongoBaseService } from './mongo-base.service';

describe('MongoBaseService', () => {
  let service: MongoBaseService;
  const id = '64cccc19487deeee6440aa69';
  const document = {
    _id: new ObjectId('64cccc19487deeee6440aa69'),
    name: 'test',
    value: 100,
  };
  let newDocument: any;

  beforeAll(async () => {
    // Create a new service instance
    service = new MongoBaseService(
      'mongodb://localhost:27017',
      'practice',
      'tests',
    );
  });

  describe('insertDocument', () => {
    it('should insert a document into the collection', async () => {
      // Create a document

      // Insert the document into the collection
      const insert = await service.insertDocument(document);

      expect(insert).toBeTruthy();
    });
  });

  describe('findDocument', () => {
    it('should find a document by _id', async () => {
      // Find the document by _id
      newDocument = await service.findDocument(id);
      expect(newDocument).toEqual(document);
    });
  });

  describe('updateDocument', () => {
    it('should update a document in the collection', async () => {
      // Update the document
      await service.updateDocument(id, { value: 200 });

      // Check that the document was updated
      newDocument = await service.findDocument(id);
      expect(newDocument.value).toEqual(200);
    });
  });

  describe('findDocuments', () => {
    it('should find all documents that match the filter', async () => {
      // Find all documents that match the filter
      const foundDocuments = await service.findDocuments({});
      expect(foundDocuments).toEqual([newDocument]);
    });
  });

  describe('deleteDocument', () => {
    it('should delete a document from the collection', async () => {
      // Delete the document from the collection
      await service.deleteDocument(id);

      // Check that the document was deleted
      const foundDocument = await service.findDocument(id);
      expect(foundDocument).toBeNull();
    });
  });
});
