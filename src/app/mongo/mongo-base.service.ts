import { Injectable } from '@nestjs/common';
import {
  Collection,
  Db,
  MongoClient,
  ObjectId,
  Document,
  InsertOneResult,
  DeleteResult,
  WithId,
  UpdateResult,
  Filter,
  OptionalId,
  FindOptions,
} from 'mongodb';

@Injectable()
export class MongoBaseService {
  #client: MongoClient;
  #databaseName: string;
  #collectionName: string;

  /**
   *  建立連線
   * @param {string} url 連線的mongoDB網址
   * @param {string} databaseName 資料庫的選擇
   * @param {string} collectionName collection的選擇
   */
  constructor(url: string, databaseName: string, collectionName: string) {
    this.#client = new MongoClient(url);
    this.#databaseName = databaseName;
    this.#collectionName = collectionName;
  }
  /**
   * 新增資料
   * @param {Document} document 新增的資料
   * @returns {Promise} 資料庫操作結果
   */
  async insertDocument(
    document: OptionalId<Document>,
  ): Promise<InsertOneResult<Document>> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.insertOne(document);

      return result;
    } finally {
      await this.#client.close();
    }
  }
  /**
   * 刪除資料
   * @param {string} id 資料的_id
   * @returns {Promise} 資料庫操作結果
   */
  async deleteDocument(id: string): Promise<DeleteResult> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.deleteOne({
        _id: new ObjectId(id),
      });

      return result;
    } finally {
      await this.#client.close();
    }
  }
  /**
   * 更新資料
   * @param {string} id 資料的_id
   * @param {Document} document 更新的資料
   * @returns {Promise} 資料庫操作結果
   */
  async updateDocument(
    id: string,
    document: Document,
  ): Promise<Document | UpdateResult> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.updateOne(
        { _id: new ObjectId(id) },
        { $set: document },
      );

      return result;
    } finally {
      await this.#client.close();
    }
  }
  /**
   * 查詢資料
   * @param {string} id 資料的_id
   * @returns {Promise} 資料庫查詢結果
   */
  async findDocument(id: string): Promise<WithId<Document>> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.findOne({
        _id: new ObjectId(id),
      });

      return result;
    } finally {
      await this.#client.close();
    }
  }
  /**
   * 查詢多筆資料
   * @param {Document} filter 過濾的條件
   * @param {Document} options 設定輸出的結果
   * @returns {Promise} 資料庫查詢結果
   */
  async findDocuments(
    filter: Filter<Document>,
    options?: FindOptions<Document>,
  ): Promise<WithId<Document>[]> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.find(filter, options).toArray();

      return result;
    } finally {
      await this.#client.close();
    }
  }
  /**
   * 聚合資料
   * @param {Document[]} pipeline pipeline的過程
   * @param {Document} options 設定輸出的結果
   * @returns {Promise} 資料庫查詢結果
   */
  async aggregateDocuments(
    pipeline?: Document[],
    options?: FindOptions<Document>,
  ): Promise<Document[]> {
    try {
      await this.#client.connect();
      const database: Db = this.#client.db(this.#databaseName);
      const collection: Collection = database.collection(this.#collectionName);

      const result = await collection.aggregate(pipeline, options).toArray();

      return result;
    } finally {
      await this.#client.close();
    }
  }
}
